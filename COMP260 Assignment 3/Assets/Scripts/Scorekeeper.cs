﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

	public int scorePerCoin = 10;
	private int[] score = new int[2];
	public Text[] scoreText;

	void Start () {
		CoinPickup[] coins = FindObjectsOfType<CoinPickup> ();

		for (int i = 0; i < coins.Length; i++) {
			coins [i].pickupEvent += onPickup;
		}

		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText[i].text = "0";
		}
	}

	public void onPickup(int player){
		score [player] += scorePerCoin;
		scoreText [player].text = score [player].ToString ();
		Debug.Log ("Player has a score of " + score [player]);
	}

	void Update () {

	}
}
